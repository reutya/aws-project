import { PhotosService } from './photos.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { FormsModule }   from '@angular/forms';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatInputModule} from '@angular/material';

import {MatDialogModule} from '@angular/material/dialog';


import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import {AngularFireStorageModule} from '@angular/fire/storage'; 
import { environment } from '../environments/environment';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';

import { RouterModule, Routes } from '@angular/router';
import { AdsComponent } from './ads/ads.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';  
import {MatTableModule} from '@angular/material/table';
import { AdformComponent } from './adform/adform.component';
import { ClassifiedComponent } from './classified/classified.component';
import { HttpClientModule } from '@angular/common/http';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatMenuModule } from '@angular/material/menu';
import { EditadComponent } from './editad/editad.component';
import { DialogAdComponent } from './dialog-ad/dialog-ad.component';
import { ChartsModule } from 'ng2-charts';
import { AlladsComponent } from './allads/allads.component';
import { LogsucComponent } from './logsuc/logsuc.component';


const appRoutes: Routes = [
  
   { path: 'ads', component: AdsComponent},
   { path: 'signup', component: SignupComponent},
   { path: 'login', component: LoginComponent},
   { path: 'adform', component: AdformComponent},
   { path: 'edit/:id', component: EditadComponent},
   { path: 'allads/:id', component: AlladsComponent},
   { path: 'classified', component: ClassifiedComponent},
   { path: 'dashboard', component: DashboardComponent},
   { path: 'allads', component:AlladsComponent},
   { path: 'suc', component: LogsucComponent},

  { path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    AdsComponent,
    LoginComponent,
    SignupComponent,
    AdformComponent,
    ClassifiedComponent,
    DashboardComponent,
    EditadComponent,
    DialogAdComponent,
    AlladsComponent,
    LogsucComponent
  ],
  entryComponents:[DialogAdComponent],
  imports: [
    // Chart,
    BrowserModule,
    ChartsModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatExpansionModule,
    MatCardModule,
    MatFormFieldModule, 
    MatSelectModule,
    HttpClientModule,
    MatInputModule,
    FormsModule,
    MatDialogModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    AngularFireStorageModule, 
    MatTableModule,
    RouterModule.forRoot(appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    MatGridListModule,
    MatMenuModule,


  ],
  providers: [AngularFireAuth,AngularFirestore,PhotosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
