export interface Photo {

    albumId: number,
    id: number,
    title: string,
    url: string,
    thumbnailUrl: string,
    image:String,
    choose:boolean,
}
