import { TestBed } from '@angular/core/testing';

import { FileUploaderService } from './file-uploader.service';

describe('FileUploaderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FileUploaderService = TestBed.get(FileUploaderService);
    expect(service).toBeTruthy();
  });
});

// import { TestBed } from '@angular/core/testing';
// import { Injectable } from '@angular/core';

// import { FileUploaderService } from './file-uploader.service';

// describe('FileUploaderService', () => {
//   let service: FileUploaderService;

//   beforeEach(() => {
//     TestBed.configureTestingModule({});
//     service = TestBed.inject(FileUploaderService);
//   });

//   it('should be created', () => {
//     expect(service).toBeTruthy();
//   });
// });
