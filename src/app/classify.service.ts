import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {
  
//  private url = "https://rniqbt3fbh.execute-api.us-east-1.amazonaws.com/beta";
  //private url = " https://8tv8azoy59.execute-api.us-east-1.amazonaws.com/beta";
  private url = "https://4yzipi32k0.execute-api.us-east-1.amazonaws.com/betaEmail";


  public categories:object = {0: 'ham', 1: 'spam'}

  public doc:string; 
  public title:string; 

  classify():Observable<any>{
    console.log(this.doc);
    let json = {
      "articles": [
        {
          "text": this.doc
        },
      ]
    }
    let body  = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log(res.body);
        let final = res.body.replace('[','');
        final = final.replace(']','');
        console.log(final);
        return final;      
      })
    );      
  }

  constructor(private http: HttpClient) { }
}
