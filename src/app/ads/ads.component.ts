import { Router } from '@angular/router';
import { element } from 'protractor';
import { DialogAdComponent } from './../dialog-ad/dialog-ad.component';
import { AngularFirestore } from '@angular/fire/firestore';
import { AdsService } from './../ads.service';
import { Component, OnInit, Inject } from '@angular/core';
import {MatTableModule} from '@angular/material/table';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { ImageService } from './../image.service';
import {MatDialog,MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig} from '@angular/material/dialog';
import { AdformComponent } from '../adform/adform.component';
import { EditadComponent } from '../editad/editad.component';
import { DatePipe } from '@angular/common';
import { PhotosService } from '../photos.service';


@Component({
  selector: 'app-ads',
  templateUrl: './ads.component.html',
  styleUrls: ['./ads.component.css']
})
export class AdsComponent implements OnInit {
  currentDate = new Date();
  $ads:Observable<any[]>;
  userId:string;
  displayedColumns1: string[] = ['num','title', 'type', 'type2','actions','photo','show','send'];
  category:string;
  ads_length:number;
  roleData:any[];
  sent:boolean;
  choose:boolean=false;
  Photos$: Observable<any>;

  
  constructor(private adservice: AdsService, private afs:AngularFirestore, public authService:AuthService
    ,public imageService:ImageService, public dialog: MatDialog, public router:Router,
    private photoService: PhotosService ) { }

  deleteAd(id){
    this.adservice.deleteAd(id,this.userId)
    console.log(id);
  }

  sendAd(id){
    this.sent=true;
    this.adservice.updateAdSent(this.userId,id,this.sent,this.currentDate);

  }

  // chooseP(id){
  //   this.router.navigate(['/allads']);
  //   this.choose=true;
  //   this.adservice.updatePhotoChoose(this.userId,id,this.choose,id);
  // }



  openDialog(body,picid,choose){
  //  this.dialog.open(EditadComponent);
  const dialogConfig = new MatDialogConfig();

      dialogConfig.disableClose = true;
      dialogConfig.autoFocus = true;

      dialogConfig.data = {
          body: body,
          picid:picid,
          choose:choose
          
  }
  const dialogRef = this.dialog.open(DialogAdComponent, dialogConfig);
  dialogRef.afterClosed().subscribe(
    data => console.log("Dialog output:", data)
);    
  }



  ngOnInit() {
    this.Photos$=this.photoService.getPhoto();

    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.$ads = this.adservice.getAds(this.userId); 
        
        //this.$ads.subscribe(result => {console.log(result.length)});
      }
    )
  }
}
