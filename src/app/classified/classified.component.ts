import { AdsService } from '../ads.service';
import { ImageService } from '../image.service';
import { ClassifyService } from '../classify.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  category:string = "Loading...";
  categoryImage:string; 
  cn:any;
  cas:object[] = [{id:0, name:'ham'},{id:1, name:'spam'}];
  
  idd: number;
  doc:string;
  id:string;
  userId:string;
  title:string;
  sent:boolean=false;
  choose:boolean=false;
  time:Date=null;
  picid:number=null;

change:boolean=false;

  onSubmit1(){
    this.doc=this.classifyService.doc;
    this.title=this.classifyService.title;

    console.log(this.userId);
    console.log(this.doc);
    console.log(this.category);
    this.adservice.addAd(this.userId,this.title,this.doc,this.category,this.sent,this.choose,this.time,this.picid);
    this.router.navigate(['/ads']);
  }
  onSubmit(){
    if (this.cn === 'ham') {
      this.idd=0;
     // alert(this.id);
    }else if (this.cn === 'spam') {
      this.idd=1;
     // alert(this.id);
    }
    console.log(this.id);
     this.category = this.classifyService.categories[this.idd];
     this.categoryImage = this.imageService.images[this.idd];
  }

change_cat(){
  this.change=true;
}

  constructor(public classifyService:ClassifyService,
              public imageService:ImageService,private adservice:AdsService, 
              private router:Router ,private route:ActivatedRoute,
              public authService :AuthService) {}



  ngOnInit() {
    this.classifyService.classify().subscribe(
      res => {
        console.log(res);
        console.log(this.classifyService.categories[res])
        this.category = this.classifyService.categories[res];
        console.log(this.imageService.images[res]);
        this.categoryImage = this.imageService.images[res];
        console.log(this.classifyService.doc)
      }
    )

    this.authService.user.subscribe(
      user=>{
        this.userId = user.uid;
       
            }
          )
       
      }
    
    
}
