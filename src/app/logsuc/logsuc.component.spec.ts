import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogsucComponent } from './logsuc.component';

describe('LogsucComponent', () => {
  let component: LogsucComponent;
  let fixture: ComponentFixture<LogsucComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogsucComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogsucComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
