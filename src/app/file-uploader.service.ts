import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})



export class FileUploaderService {
  // filename:string="value1";
  // user:string="value2";

  constructor(private http : HttpClient) { }
  // My Students: please replace the post url below 
  fileUpload(file: FormData) {
      return this.http.post('http://ec2-18-209-179-70.compute-1.amazonaws.com/upload-ng', file);
  }

  callRek() {
    return this.http.get('http://ec2-18-209-179-70.compute-1.amazonaws.com/rekognition');

  }
  callGetItem(filename,user) {
    return this.http.get('http://ec2-18-209-179-70.compute-1.amazonaws.com/getitem?filename='+filename+'&user='+user);
  }

  callGetItems(filename) {
    return this.http.get('http://ec2-18-209-179-70.compute-1.amazonaws.com/getitems?filename='+filename);
  }

  callSetItem(item: any){
    return this.http.post('http://ec2-18-209-179-70.compute-1.amazonaws.com/setitem',item);
  }
}
