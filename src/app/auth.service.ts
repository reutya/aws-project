import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { User } from './interfaces/user';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { error } from '@angular/compiler/src/util';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User|null>
  private logInErrorSubject = new Subject<string>();

  public getLoginErrors():Subject<string>{
    return this.logInErrorSubject;
  }

  constructor(public afAuth:AngularFireAuth,
    private router:Router,) {
    this.user = this.afAuth.authState;
}

getUser(){
  return this.user
}

doRegister(value){
  return new Promise<any>((resolve, reject) => {
    this.afAuth.auth.createUserWithEmailAndPassword(value.email, value.password)
    .then(res => {
      resolve(res);
    }, err => reject(err))
  })
}

dologin(value){
  return new Promise<any>((resolve, reject) => {
    this.afAuth.auth.signInWithEmailAndPassword(value.email, value.password)
    .then(res => {
      resolve(res);
    }, err => reject(err))
  })
}


  SignUp(email:string, password:string){
    this.afAuth
        .auth
        .createUserWithEmailAndPassword(email,password)
        .then(res => 
          {
            console.log('Succesful sign up',res);
           this.router.navigate(['/dashboard']);
          }
        );
  }

  Logout(){
    this.afAuth.auth.signOut();
    this.router.navigate(['/login']);
  }
  login(email:string, password:string){
    this.afAuth
        .auth.signInWithEmailAndPassword(email,password)
        .then(() => this.router.navigate(['/dashboard']).catch(error => window.alert(error)))
        // .then( res => 
        //   {
        //     console.log('Succesful Login',res);
        //     this.router.navigate(['/ads']);
        //   }
        // )
  }


  // bar chart
  
}
