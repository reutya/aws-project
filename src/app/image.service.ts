import { Injectable } from '@angular/core';
 
@Injectable({
 providedIn: 'root'
})
export class ImageService {
 
 public images:string[] = [];
 public path:string = 'https://firebasestorage.googleapis.com/v0/b/project-aec9c.appspot.com/o/'

constructor() {
 this.images[0] = this.path +'ham.png' + '?alt=media';
 this.images[1] = this.path +'spam.jpg' + '?alt=media';
}
 
}


