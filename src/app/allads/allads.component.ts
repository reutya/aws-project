import { AuthService } from './../auth.service';
import { AdsService } from './../ads.service';
import { Photo } from './../interface/photo';
import { PhotosService } from './../photos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-allads',
  templateUrl: './allads.component.html',
  styleUrls: ['./allads.component.css']
})
export class AlladsComponent implements OnInit {

  //  Photos$: Observable<any[]>;
  Photos$: Observable<any>;
  choose:boolean=false;
  userId:string;
 id_ad:string;
 picid:number;
  constructor(private route:ActivatedRoute, public router:Router, private photoService: PhotosService
    , public adsService:AdsService, public authService:AuthService ) { }



chooseP(id){
  this.choose=true;
  this.adsService.updatePhotoChoose(this.userId,this.id_ad,this.choose,id);
  this.router.navigate(["/ads"]);

}

  ngOnInit() {

    // return this.photoService.getPhoto()
    // .subscribe(data =>this.Photos$ = data); 
    this.Photos$=this.photoService.getPhoto();
    
    if (this.route.snapshot.params.id === undefined)
    {
      this.id_ad='0';
    }
    else{
      this.id_ad=this.route.snapshot.params.id;
    }
    console.log(this.id_ad)
    this.authService.user.subscribe(
      user=>{
        this.userId = user.uid;
          this.adsService.getAd(this.userId,this.id_ad).subscribe(
            ad=>{
              this.picid=ad.data().picid;
             
            }

          ) 
        
          console.log(this.id_ad);
        }

        )




  }

}
