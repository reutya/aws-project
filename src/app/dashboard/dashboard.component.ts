import { DashboardService } from './../dashboard.service';
import { AuthService } from './../auth.service';
import { Component } from '@angular/core';
import { map, count } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { Chart } from 'chart.js';
import { AdsService } from '../ads.service';
import { MultiDataSet, Label } from 'ng2-charts';
import { ChartType } from 'chart.js';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { FileUploaderService } from '../file-uploader.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {
  
  ans_word:string = "";
  msg:string;
  num_person:number;
  num_after:string;
  arrayJason:any;
  arrayPerson:any;
  fileObj: File;
  fileUrl: string;
  errorMsg: boolean;
  title1 = 'Check Your Space';
  itemJason:any;
  all:any;
  i:any;
  title: string;
  data_length:number;
  $items:Observable<any[]>;
  constructor(private adservice: AdsService,private breakpointObserver: BreakpointObserver,
     public authservice:AuthService,
      public dashboardService:DashboardService, private db: AngularFirestore,private router:Router ,
      private fileUploadService: FileUploaderService) {
        this.errorMsg = false

  }

 
userId:string;
userEmail:string;


count_person(){
    this.fileUploadService.callRek().subscribe(
      (res) =>(
        this.arrayJason = res,
        console.log(this.arrayJason),
        console.log(this.arrayJason.Labels),
        this.testr(),
        this.ok()
  
      )
    );

  }

  testr(){
    for (let index = 0; index < this.arrayJason.Labels.length; index++) {
      if (this.arrayJason.Labels[index].Name=="Person") {
          console.log(this.arrayJason.Labels[index]);
          this.arrayPerson=this.arrayJason.Labels[index];
          console.log("this is person array",this.arrayPerson.Instances.length);
          this.num_person=this.arrayPerson.Instances.length;
          console.log(this.num_person)
          break
        
      }          
    }
  }

  ok(){
    if (this.num_person<20 && this.num_person>10) {
      this.msg=" Your space is safe"
    }
    else if(this.num_person<11){
      this.msg=" Your space complies with regulations"
    }
    else if (this.num_person>20 && this.num_person<1000) {
      this.msg=" Your space is too crouded"
      
    }
  }

  onFilePicked(event: Event): void {

    this.errorMsg = false;
    console.log(event);
    const FILE = (event.target as HTMLInputElement).files[0];
    this.fileObj = FILE;
    console.log(this.fileObj);
  }
  
  onFileUpload() {

    if (!this.fileObj) {
      this.errorMsg = true;
      return
    }
    
    const fileForm = new FormData();
    fileForm.append('file', this.fileObj);
    this.fileUploadService.fileUpload(fileForm).subscribe(res => {
      console.log('RESPONSE FROM SERVER UPLOAD');
      console.log(res);
      this.fileUrl = res['image'];
      this.count_person();

    });
  }

  getItem(filename:string,user:string){
    this.fileUploadService.callGetItem(filename,user).subscribe(
      (res) =>(
        this.itemJason = res,
        console.log(this.itemJason)
  
      )
    );
  }

  getItems(filename:string){
    this.fileUploadService.callGetItems(filename).subscribe(
      (res) =>(
        this.all = res,
        console.log(this.all),
        console.log(this.all.data.Items),
        this.$items=this.all.data.Items
        // this.data_length=this.all.data.Items.length,
        // console.log("the length:",this.data_length)

  
      )
    );
  }
  additem(){
   
    this.num_after=this.num_person.toString()
    const item= {file_name:this.userEmail,user:this.title,num_p:this.num_after,comment_p:this.msg};
    this.fileUploadService.callSetItem(item).subscribe(
      (res) =>(
        this.i = res,
        console.log(res)

      )
    );
 this.router.navigate(['/suc']);
  }
 ngOnInit() {
  this.authservice.user.subscribe(
    user => {
      this.userId = user.uid;
      this.userEmail = user.email;
      console.log(this.userEmail);
      this.getItems(this.userEmail);

     })
    }


 
}
