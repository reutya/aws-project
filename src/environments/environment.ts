// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCeSYzAqS_FlvT26edbgv46IJL0hwzS2eU",
    authDomain: "project-aec9c.firebaseapp.com",
    databaseURL: "https://project-aec9c.firebaseio.com",
    projectId: "project-aec9c",
    storageBucket: "project-aec9c.appspot.com",
    messagingSenderId: "416307940576",
    appId: "1:416307940576:web:b7265c7f0bb911adfbd485",
    measurementId: "G-J6BW4CTK6R"
  }  
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
